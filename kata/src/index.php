<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/Entity/Destination.php';
require_once __DIR__ . '/Entity/Quote.php';
require_once __DIR__ . '/Entity/Site.php';
require_once __DIR__ . '/Entity/Template.php';
require_once __DIR__ . '/Entity/User.php';
require_once __DIR__ . '/Helper/SingletonTrait.php';
require_once __DIR__ . '/Context/ApplicationContext.php';
require_once __DIR__ . '/Repository/AbstractRepository.php';
require_once __DIR__ . '/Repository/Repository.php';
require_once __DIR__ . '/Repository/UserRepository.php';
require_once __DIR__ . '/Repository/DestinationRepository.php';
require_once __DIR__ . '/Repository/QuoteRepository.php';
require_once __DIR__ . '/Repository/SiteRepository.php';
require_once __DIR__ . '/TemplateManager.php';

$faker = \Faker\Factory::create();

$template = new Template(
    1,
    'Votre voyage avec une agence locale [quote:destination_name]',
    "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].

[quote:destination_link]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
$templateManager = new TemplateManager();

$message = $templateManager->getTemplateComputed(
    $template,
    [
        'quote' => new Quote($faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber(), $faker->date())
    ]
);

echo $message->getSubject() . "\n" . $message->getContent();
