<?php

class TemplateManager
{

    /**
     *  Context.
     *
     * @var null
     */
    protected static $context = null;

    /**
     * @return null
     */
    public static function getContext()
    {
        if (empty(self::$context)) {
            self::setContext();
        }
        return self::$context;
    }

    /**
     * @param null $context
     */
    public static function setContext($context = null)
    {
        if (empty($context)) {
            $context = ApplicationContext::getInstance();
        }
        self::$context = $context;
    }


    /**
     * @var array
     */
    protected $templateProperties = [
        'subject',
        'content',
    ];

    public static function init() {
        self::setContext();
    }

    /**
     * TemplateManager constructor.
     */
    public function __construct()
    {
        self::init();
    }

    /**
     * Get template computed.
     *
     * @param Template $template
     *   Tempalce instane.
     * @param array $data
     *
     * @return Template
     */
    public function getTemplateComputed(Template $template, array $data)
    {
        if (!$template instanceof Template) {
            throw new \RuntimeException('No template given.');
        }

        return $this->computeTemplateProperties($template, $data);
    }

    /**
     * @param Template $template
     * @param array $data
     * @return Template $newTemplate;
     */
    private function computeTemplateProperties(Template $template, array $data) {

        // Clone template instance.
        $newTemplate = clone($template);

        foreach ($this->getTemplateProperties() as $property) {
            $newTemplate
                ->{'set' . ucfirst($property)} (
                    $this->computeText($newTemplate->{'get' . ucfirst($property)}(), $data)
                );
        }

        return $newTemplate;
    }

    /**
     * @return array
     */
    public function getTemplateProperties()
    {
        return $this->templateProperties;
    }

    /**
     * Set template properties.
     *
     * @param array $template_properties
     * @return $this
     */
    public function setTemplateProperties(array $template_properties)
    {
        $this->templateProperties = $template_properties;

        return $this;
    }

    private function computeText($text, array $data)
    {

        $context = self::getContext();

        // Process user
        $user  = (isset($data['user']) and ($data['user']  instanceof User))  ? $data['user']  : $context->getCurrentUser();

        UserRepository::getInstance()->tokenToggle(['user', 'first_name'], ucfirst(mb_strtolower($user->getFirstname())), $text);

        // Process quote
        $quote = (isset($data['quote']) and $data['quote'] instanceof Quote) ? $data['quote'] : null;
        if (empty($quote)) {
            return;
        }


        $destinationRepository = DestinationRepository::getInstance();

        $destination = $destinationRepository->getById($quote->getDestinationId());
        $site = SiteRepository::getInstance()->getById($quote->getSiteId());

        $link = $site->getUrl() . '/' . $destination->getCountryName() . '/quote/' . $quote->getId();

        $destinationRepository
            ->tokenToggle(['quote', 'destination_link'], $link, $text)
            ->tokenToggle(['quote', 'destination_name'], $destination->getCountryName(), $text);

        QuoteRepository::getInstance()
            ->tokenToggle(['quote', 'summary_html'], Quote::renderHtml($quote), $text)
            ->tokenToggle(['quote', 'summary'], Quote::renderText($quote), $text);

        return $text;
    }
}
