<?php

class ApplicationContext
{
    use SingletonTrait;
    /**
     *  Data handler.
     *
     * @var null
     */
    protected static $dataHandler = null;

    /**
     * @var Site
     */
    private $currentSite;

    /**
     * @var User
     */
    private $currentUser;

    protected static $init = false;
    /**
     * @return null
     */
    public static function getDataHandler()
    {
        if (empty(self::$dataHandler)) {
            self::setDataHandler();
        }
        return self::$dataHandler;
    }

    /**
     * @param null $dataHandler
     */
    public static function setDataHandler($dataHandler = null)
    {
        if (empty($dataHandler)) {
            $dataHandler = \Faker\Factory::create();
        }
        self::$dataHandler = $dataHandler;
    }

    public function init() {
        if (self::$init) return;
        self::$init = true;
        $dataHandler = self::getDataHandler();
        $this
            ->setCurrentSite(new Site($dataHandler->randomNumber(), $dataHandler->url))
            ->setCurrentUser(new User($dataHandler->randomNumber(), $dataHandler->firstName, $dataHandler->lastName, $dataHandler->email));
}
    /**
     * ApplicationContext constructor.
     */
    public function __construct() {
        $this->init();
    }

    /**
     * @return Site
     */
    public function getCurrentSite()
    {
        return $this->currentSite;
    }

    /**
     * @param Site $currentSite
     * @return $this
     */
    public function setCurrentSite($currentSite)
    {
        $this->currentSite = $currentSite;
        return $this;
    }

    /**
     * @return User
     */
    public function getCurrentUser()
    {
        return $this->currentUser;
    }

    /**
     * @param User $currentUser
     * @return $this
     */
    public function setCurrentUser($currentUser)
    {
        $this->currentUser = $currentUser;
        return $this;
    }
}
