<?php

class UserRepository extends AbstractRepository implements Repository
{
    use SingletonTrait;

    /**
     * @var int
     */
    protected $userId;

    /**
     * @var string
     */
    protected $firstname;

    /**
     * @var string
     */
    protected $lastname;

    /**
     * @var array
     */
    protected $tokens = [
        '[user:first_name]',
    ];

    /**
     * QuoteRepository constructor.
     */
    public function __construct()
    {
        // DO NOT MODIFY THIS METHOD
        $generator = Faker\Factory::create();

        $this->userId = $generator->numberBetween(1, 10);
        $this->firstname = $generator->firstName;
        $this->lastname = $generator->lastName;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function getById($id)
    {
        // DO NOT MODIFY THIS METHOD
        return new User(
            $id,
            $this->userId,
            $this->firstname,
            $this->lastname
        );
    }
}
