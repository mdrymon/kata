<?php

abstract class AbstractRepository {

    /**
     * List of allowed tokens.
     *
     * @var array
     */
    protected $tokens = [];

    /**
     * Token toggle.
     *
     * @param array $token
     * @param $text
     * @return $this
     */
    public function tokenToggle(array $token, $replace, &$text) {
        $token = '[' . implode(':', $token) . ']';
        if (!in_array($token, $this->tokens)) {
            throw new \RuntimeException('Not allowed token.');
        }
        $text = str_replace(
            $token,
            (string) $replace,
            (string) $text
        );

        return $this;
    }

}