<?php

class DestinationRepository extends AbstractRepository implements Repository
{
    use SingletonTrait;

    /**
     * @var string
     */
    protected $country;

    /**
     * @var string
     */
    protected $conjunction;

    /**
     * @var string
     */
    protected $computerName;

    /**
     * @var array
     */
    protected $tokens = [
        '[quote:destination_link]',
        '[quote:destination_name]',
    ];

    /**
     * DestinationRepository constructor.
     */
    public function __construct()
    {
        $this->country = Faker\Factory::create()->country;
        $this->conjunction = 'en';
        $this->computerName = Faker\Factory::create()->slug();
    }

    /**
     * @param int $id
     *
     * @return Destination
     */
    public function getById($id)
    {
        // DO NOT MODIFY THIS METHOD
        return new Destination(
            $id,
            $this->country,
            $this->conjunction,
            $this->computerName
        );
    }
}
